# collegiate-dictionary
This command line utility provide you the word defination by quering the 
[merriam-webster dictionary](https://dictionaryapi.com/products/api-collegiate-dictionary)


## Getting started

# Local Debugging
First install the app locally and set env var


```bash
export API_KEY=''
 
cd app
pip3 install . --user

python main.py -w “exercise”

