'''Setup script'''

import os
from setuptools import setup, find_packages

def read(fname):
    '''Utility function to read the contents of a file.'''
    return open(os.path.join(os.path.dirname(__file__), fname)).read()



setup(name='Word Dictionary',
      version="0.0.1",
      description='Word Dictionary',
      author='Sudhir Mudpe',
      packages=find_packages()
      )
