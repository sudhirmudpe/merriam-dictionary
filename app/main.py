# importing the required modules
import os
import argparse
from WordDictonary import generateWordAndDefenition

# error messages


def read(args):
	# get the word
    input_word = args
    input_word = generateWordAndDefenition(input_word)
    # if(input_word):
    #     for i in range(len(input_word.definitions)):
    #             if len(input_word.definitions)>1:
    #                  print(input_word.definitions[i]["definition"])
    if(input_word):
        print(input_word.definitions[0]["definition"][0])
    else:
        print('no data found')

def main():
	# create parser object
    parser = argparse.ArgumentParser(description = "A Word Dictionary!")
    # defining arguments for parser object
    parser.add_argument("-w", "--words", type = str, 
                        metavar = "word", default = None,
                        help = "Enter the word for defination.")

    # parse the arguments from standard input
    args = parser.parse_args()

    if args.words != None:
        read(args.words)


if __name__ == "__main__":
	# calling the main function
	main()
