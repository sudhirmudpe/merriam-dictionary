import requests
import json
from string import ascii_lowercase

class Word:
    def __init__(self, name):
        self.name = name
        self.definitions = []


def getDefinition(word):
    KEY = "9d9bbe90-88a3-4cc1-bfbb-16521e40a058" #os.getenv("API_KEY") #used in API call
    print(word)
    requestURL = 'https://dictionaryapi.com/api/v3/references/collegiate/json/'+ str(word) +'?key=' + KEY 
    apiResponse = requests.get(requestURL) #http GET request of URL using requests library
    # apiResponse.decode = 'utf-8'
    return json.loads(apiResponse.content)

def parseJson(word_entry):
    word_dict = {}
    definition_dict = {}
    
    try:
        word_dict['class'] = word_entry['fl'] 
    except Exception as e:
        word_dict['class'] = 'No Class Type Specified'
    if(word_entry['fl'] == 'noun'):
        for index, definition in enumerate(word_entry['shortdef']):
            definition_dict[index] = definition
    word_dict['definition'] = definition_dict
    return(word_dict)

def generateWordAndDefenition(input_word):
    word_info = getDefinition(input_word)
    ourWord = Word(input_word)
    if not any(isinstance(info, dict) for info in word_info): 
        ourWord = {}
    else:
        # print(word_info)
        definitions_list = []
        if 'hom' in word_info[0]: 
            definitions_list = [parseJson(entry) for entry in word_info if entry.get('shortdef') and entry.get('hom')] 
            ourWord.definitions = definitions_list
        elif word_info[0].get('shortdef'):
            definitions_list  = [parseJson(word_info[0])]
            ourWord.definitions = definitions_list
    return ourWord


